var session = require('express-session');
var cookieSession = require('cookie-session');
var express = require('express');
var app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
const flash = require('express-flash-messages');
const compression = require('compression');
global._ = require('lodash');
global.fetch = require("node-fetch");
global.qs = require("querystrings");
global.path = require('path');
global.os = require('os');

app.use(cookieSession({
   name: 'session',
   keys: ['secret'],
   httpOnly: false,
   // Cookie Options
   maxAge: 1000 * 3600 * 24 * 30 * 2 //1000(milsecs) * 3600(mins) * 24(hours) * 30(days) * 2(months) 
}));

//console.log(os);
//console.log(os.hostname());

app.use(function (req, res, next) {

   //req.session = null;
   // session id
   if (!req.session.user_uniqid) {
      req.session.user_uniqid = utls.getToken();
   }
   // favourite items
   //req.session.favitems = [];
   if (req.session.user_uniqid && !utls.isEmpty(req.session.favitems)) {
      req.session.favitems = [];
   }
   // create cart items
   //req.session.cartitems = [];
   if (req.session.user_uniqid && !utls.isEmpty(req.session.cartitems)) {
      req.session.cartitems = [];
   }

   if (typeof req.session.auth == 'undefined') {
      req.session.auth = {};
   }

   next();
});

app.use(flash());

//app.locals.favitems = req.session.favitems;

/**
 * Store database credentials in a separate config.js file
 * Load the file/module and its values
 */
global.config = require('./config');
global.utls = require('./utils/utils');

var dbOptions = {
   host: config.db.host,
   user: config.db.user,
   password: config.db.password,
   port: config.db.port,
   database: config.db.db,
   charset: config.db.charset
}

/*global.conn = mysql.createConnection(dbOptions);

//connect to database
conn.connect((err) => {
   if (err) {
      throw err;
   }
   console.log('Mysql Connected...');
});*/

// port number
var port = process.env.PORT || config.server.port;

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(bodyParser.urlencoded({ extended: true }));
// Parse JSON bodies (as sent by API clients)
app.use(bodyParser.json());

var jsondata = { test: "testingnew" };

global.wcapi = new WooCommerceRestApi({
   url: config.wcsettings.url,
   consumerKey: config.wcsettings.consumerKey,
   consumerSecret: config.wcsettings.consumerSecret,
   version: config.wcsettings.version
});

app.use(compression()); // Compress all Responses
app.use(express.static(__dirname + '/assets')); //dirname gives the current directory 
app.set('view engine', 'ejs'); // set the view engine to ejs

/*app.get('/', async function (req, res) {

   if (req.session.auth.hasOwnProperty('id')) {
      console.log('user data avail');
   }
   
   console.log('+++++++++++++++');
   console.log(req.session.user_uniqid);
   console.log(req.session.favitems);
   console.log(req.session.cartitems);
   console.log(req.session.auth);
   console.log('+++++++++++++++');
   
   
   let reqdata = {
      status: 'publish',
      per_page: 9,
      page: 1,
      orderby: "date",
      order: "desc"
   }

   let products = await getWCApiAsync("products", reqdata);
   let categories = await getWCApiAsync("products/categories");
   let tags = await getWCApiAsync("products/tags");

   data = {
      products: products,
      categories: utls.getNestedChildren(categories, 0),
      tags: tags,
      customeScript: {enable: true}
   }
   req.flash('notify', 'Redirect successful!');
   res.render("shop/pages/shop", data);

});*/

app.get('/', async function (req, res) {
   if (req.session.auth.hasOwnProperty('id')) {
      console.log('user data avail');
   }
   console.log('+++++++++++++++');
   console.log(req.session.user_uniqid);
   console.log(req.session.favitems);
   console.log(req.session.cartitems);
   console.log(req.session.auth);
   console.log('+++++++++++++++');
   let categories = await getWCApiAsync("products/categories");
   data = {
      categories: utls.getNestedChildren(categories, 0)
   }
   req.flash('notify', 'Redirect successful!');
   res.render("shop/pages/homepage", data);
});

//async function getWCApiAsync(endpoint, data = {}) {
global.getWCApiAsync = async function (endpoint, data = {}) {
   try {
      let items = await wcapi.get(endpoint, data);
      let results = await items;
      return results.data;
   } catch (error) {
      console.log(error.response.data);
      return error.response.data;
   }
}

//async function postWCApiAsync(endpoint, data = {}) {
global.postWCApiAsync = async function (endpoint, data = {}) {
   try {
      let items = await wcapi.post(endpoint, data);
      let results = await items;
      return results.data;
   } catch (error) {
      console.log(error.response.data);
      return error.response.data;
   }
}

//async function putWCApiAsync(endpoint, data = {}) {
global.putWCApiAsync = async function (endpoint, data = {}) {
   try {
      let items = await wcapi.post(endpoint, data);
      let results = await items;
      return results.data;
   } catch (error) {
      console.log(error.response.data);
      return error.response.data;
   }
}

app.get('/test', async function (req, res) {

   console.log(req.query);

   filter = {
      orderby: "meta_value_num", orderby_meta_key: "_regular_price", order: "desc"
   };

   let reqdata = {
      status: 'publish',
      per_page: 9,
      page: 1,
      orderby: "date",
      order: "desc"
   }

   let products = await getWCApiAsync("products", reqdata);
   let categories = await getWCApiAsync("products/categories");
   let tags = await getWCApiAsync("products/tags");

   data = {
      products: products,
      categories: utls.getNestedChildren(categories, 0),
      tags: tags
   }

   res.render("shop/pages/shop", data);

});

// router connection
var apiroutes = require('./routes/index');
app.use(apiroutes); // Routes initiate in middleware

/* catch 404 and forward to error handler */
/*app.use(function (req, res, next) {
   var err = new Error('Sorry Your Request Is Not Found!.....');
   err.status = 404;
   next(err);
});*/

/* error handler */
/*app.use(async function (err, req, res, next) {
   // set locals, only providing error in development
   res.locals.message = err.message;
   res.locals.error = req.app.get('env') === 'development' ? err : {};

   console.log(err);

   // render the error page
   res.status(err.status || 500);
   //let products = await getWCApiAsync("products");
   let categories = await getWCApiAsync("products/categories");
   data = {
      products: null,
      categories: utls.getNestedChildren(categories, 0),
   }
   res.render("shop/pages/404", data);
});*/

// Assigning Port
app.listen(port, function () {
   console.log('Server listening on port ' + port + '...');
});
