$(document).ready(function () {

    $("#jquery-accordion-menu").jqueryAccordionMenu();
    $(".colors a").click(function () {
        if ($(this).attr("class") != "default") {
            $("#jquery-accordion-menu").removeClass();
            $("#jquery-accordion-menu").addClass("jquery-accordion-menu").addClass($(this).attr("class"));
        } else {
            $("#jquery-accordion-menu").removeClass();
            $("#jquery-accordion-menu").addClass("jquery-accordion-menu");
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
            $('.scrollup').addClass('active');
        } else {
            $('.scrollup').fadeOut();
            $('.scrollup').removeClass('active');
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $('[data-toggle="tooltip"]').tooltip();

    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    $('.modal').on('show.bs.modal', reposition);
    $(window).on('resize', function () {
        $('.modal:visible').each(reposition);
    });

    $(window).scroll(function () {
        var menu_h = $('.header_login').height();
        if ($(this).scrollTop() > menu_h) {
            $('.bottom_fixed_header').removeClass('active');
        } else {
            $('.bottom_fixed_header').addClass('active');
        }
        if ($(window).width() < 767) {
            $('.bottom_fixed_header').removeClass('active');
        }
    });

    var menu_h = $('.header_login').height();
    //$('.banner-slider').css({ "marginTop": menu_h });
    $('.topCommonPadding').css({ "marginTop": menu_h });


    $(window).scroll(function () {
        var banner_h = $('.banner-slider').height();
        var menu_h = $('.header_login').height();
        var scroll = $(window).scrollTop();
        //alert(searh_h);
        if (scroll > banner_h) {
            $(".leftFilterSticky").addClass('active');
            $('.leftFilterSticky').css({ "top": menu_h });

            $(".sortSection").addClass('active');
            $('.sortSection').css({ "top": menu_h });

        }
        else {
            $(".leftFilterSticky").removeClass('active');
            $('.leftFilterSticky').css({ "top": "0px" });

            $(".sortSection").removeClass('active');
            $('.sortSection').css({ "top": "0px" });
        }
        if ($(window).width() < 767) {
            $(".leftFilterSticky").removeClass('active');
        }
    });

    $('#product-detail').eagleGallery({
        miniSliderArrowPos: 'inside',
        miniSliderArrowStyle: 2,
        changeMediumStyle: true,
        autoPlayMediumImg: true,
        openGalleryStyle: 'transform',
        bottomControlLine: true,
        theme: 'light',
        miniSlider: {
            navigation: true,
            pagination: false,
            navigationText: false,
            rewindNav: false,
            theme: 'mini-slider',
            responsiveBaseWidth: '.eagle-gallery',
            itemsCustom: [[0, 1], [250, 2], [450, 5], [650, 5], [850, 5], [1050, 6], [1250, 7], [1450, 8]],
            margin: 1
        }
    });


    $('.banner-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });


    $(document).on('click', '#mobileCartPopupOpen', function () {
        $('#cartPopup').addClass('active');
        $('.popUp-backdrop').addClass('show');
    });

    $(document).on('click', '#cartPopupClose', function () {
        $('#cartPopup').removeClass('active');
        $('.popUp-backdrop').removeClass('show');
    });


    $(document).on('click', '#cartPopupOpen', function () {
        $('#cartPopup').addClass('active');
        $('.popUp-backdrop').addClass('show');
    });
    $(document).on('click', '#cartPopupClose', function () {
        $('#cartPopup').removeClass('active');
        $('.popUp-backdrop').removeClass('show');
    });
    $(document).on('click', '.popUp-backdrop', function () {
        $('#cartPopup').removeClass('active');
        $(this).removeClass('show');
    });


    $('#mobileNavPopupOpen').on('click', function () {
        $('.leftFilterStickyy').addClass('active');
        $('.popUp-backdrop').addClass('show');
    });
    $('.popUp-backdrop').on('click', function () {
        $('.leftFilterStickyy').removeClass('active');
        $(this).removeClass('show');
    });


    $('.mobileFiterOpen').on('click', function () {
        $('.leftFilterSticky').addClass('Leftactive');
        $('.popUp-backdrop').addClass('show');
    });
    $('.mobFilterClose').on('click', function () {
        $('.leftFilterSticky').removeClass('Leftactive');
        $('.popUp-backdrop').removeClass('show');
    });
    $('.popUp-backdrop').on('click', function () {
        $('.leftFilterSticky').removeClass('Leftactive');
        $(this).removeClass('show');
    });

    $('#mobileNavPopupOpen').on('click', function () {
        $('.leftFilterSticky').addClass('Leftactive');
        $('.popUp-backdrop').addClass('show');
        $('.leftFilterSticky').addClass('mobile-menu');
    });
    $('.mobFilterClose').on('click', function () {
        $('.leftFilterSticky').removeClass('mobile-menu');
    });
    $('.popUp-backdrop').on('click', function () {
        $('.leftFilterSticky').removeClass('mobile-menu');
    });

    $(document).on('click', '#favOpen', function () {
        $('#wishpopup').addClass('active');
        $('.popUp-backdrop').addClass('show');
    });
    $(document).on('click', '#favClose', function () {
        $('#wishpopup').removeClass('active');
        $('.popUp-backdrop').removeClass('show');
    });
    $(document).on('click', '.popUp-backdrop', function () {
        $('#wishpopup').removeClass('active');
        $(this).removeClass('show');
    });

    $('.testi-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $(".accordion_head").click(function () {
        if ($('.accordion_body').is(':visible')) {
            //$(".accordion_body").slideUp();
            $(".plusminus").html('<i class="fa fa-angle-right"></i>');
        }
        if ($(this).next(".accordion_body").is(':visible')) {
            $(this).next(".accordion_body").slideUp();
            $(this).children(".plusminus").html('<i class="fa fa-angle-right"></i>');
        } else {
            $(this).next(".accordion_body").slideDown();
            $(this).children(".plusminus").html('<i class="fa fa-angle-down"></i>');
        }
    });

    $(".form-scrolling").mCustomScrollbar({
        theme: "light"
    });

    $(".verTrigger").click(function(){
        $(".versanddetails").slideToggle(500);
    });

    jQuery("#PostRating").rating({
        "value": 2,
        "click": function (e) {
            console.log(e);
            $("#starsInput").val(e.stars);
        }
    });

    //header scroller
    $('.header-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    //category scroller
    $('.category-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: false,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    //sub-category scroller
    $('.sub-category-slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: false,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });


});

$(window).on('load', function () {
    $(".form-scrolling").mCustomScrollbar({
        theme: "light"
    });
});

jQuery(document).on('click', '.pcatone', function () {
    jQuery(this).closest('.catItem').addClass('active');
    var getOpenItem = jQuery(this).attr('data-id');
    var menu_h = $('.header_login').height();
    var stepone = jQuery('.homeStepOne');
    var steptwo = jQuery('.homeStepTwo');
    stepone.addClass('active');
    setTimeout(function(){ jQuery('html,body').animate({scrollTop: steptwo.offset().top - menu_h - 25},'slow'); }, 500);
    jQuery('.secondLevel, .thirdLevel').hide();
    jQuery('#'+getOpenItem).addClass('active').show();
    jQuery('.header-slider').css({opacity: 1});
    jQuery('.owl-item').find('.item').removeClass('active');
    jQuery('.owl-item').find('.'+getOpenItem).addClass('active');
    console.log(getOpenItem);
});

jQuery(document).on('click', '.pcatwo', function () {
    jQuery(this).closest('.catItem').addClass('active');
    var getOpenItem = jQuery(this).attr('data-id');
    var menu_h = $('.header_login').height();
    var steptwo = jQuery('.homeStepTwo');
    var stepThree = jQuery('.homeStepThree');
    steptwo.addClass('active');
    setTimeout(function(){ jQuery('html,body').animate({scrollTop: stepThree.offset().top - menu_h - 25},'slow'); }, 500);
    jQuery('.thirdLevel').hide();
    jQuery('#'+getOpenItem).addClass('active').show();
    console.log(getOpenItem);
});

