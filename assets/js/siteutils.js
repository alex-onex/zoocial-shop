
async function getData(url = '', restype = 'text') {
    try {
        const response = await fetch(url);
        return (restype == 'json') ? response.json() : response.text();
    } catch (error) {
        console.error('Error:', error);
    }
}

async function postData(url = '', inputdata = {}, restype = 'json') {

    var method = inputdata.method;

    try {
        const response = await fetch(url, {
            method: method, // or 'PUT'
            body: JSON.stringify(inputdata.data), // data can be `string` or {object}!
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (restype == 'json') {
            const jsondata = await response.json();
            console.log('Success:', JSON.stringify(jsondata));
            return jsondata;
        } else {
            const textdata = await response.text();
            console.log('Success:', textdata);
            return textdata;
        }
        //console.log('Success:', JSON.stringify(response));
        //return (restype == 'json') ? response.json() : response.text();
    } catch (error) {
        console.error('Error:', error);
    }
}

jQuery(window).on('load', function () {

    getData('/wishmodule', 'text')
        .then((content) => {
            jQuery('#wishpopup').empty().append(content);
            //console.log(jQuery('.wishToplist ul li').length);
            var productcount = parseInt(jQuery('.wishToplist ul li').length) > 0 ? parseInt(jQuery('.wishToplist ul li').length) : 0;
            jQuery('.headerCart .fav-count').empty().text(productcount);
        })
        .catch((err) => {
            console.log('wish module error:' + err);
        });

    getData('/cartmodule', 'text')
        .then((content) => {
            jQuery('#cartPopup').empty().append(content);
            //console.log(jQuery('.cartToplist ul li').length);
            var productcount = parseInt(jQuery('.cartToplist ul li').length) > 0 ? parseInt(jQuery('.cartToplist ul li').length) : 0;
            jQuery('.headerCart .product_count').empty().text(productcount);
            //console.log(content);
        })
        .catch((err) => {
            console.log('wish module error:' + err);
        });

    getData('/usermenus', 'text')
        .then((content) => {
            jQuery('.usermenus').empty().append(content);
        })
        .catch((err) => {
            console.log('usermenus module error:' + err);
        });

});

$(document).ready(function () {

    // Add Favourite
    jQuery(document).on('click', '.addfavourite', function () {

        var productid = jQuery(this).attr('product-id');
        const url = '/wishlist';
        const inputdata = {
            method: 'POST',
            data: {
                id: productid
            }
        };
        postData(url, inputdata, 'json')
            .then((json) => {
                console.log(json);
                if (json.status) {
                    jQuery('.fav-count').empty().text(json.items.length);
                    getData('/wishmodule', 'text')
                        .then((content) => {
                            jQuery('#favOpen').trigger('click');
                            jQuery('#wishpopup').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('wish module error:' + err);
                        });
                }
            })
            .catch((error) => {
                console.log('Error:' + error);
            });
    });

    // Remove Wish Item
    jQuery(document).on('click', '.remove_wish', function () {

        jQuery(this).closest('.wishlist-item').remove();
        var productid = jQuery(this).attr('product-id');
        console.log(productid);
        const url = '/wishlist';
        const inputdata = {
            method: "DELETE",
            data: {
                id: productid
            }
        };
        postData(url, inputdata, 'json')
            .then((json) => {
                console.log(json);
                if (json.status) {
                    jQuery('.fav-count').empty().text(json.items.length);
                    getData('/wishmodule', 'text')
                        .then((content) => {
                            jQuery('#wishpopup').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('wish module error:' + err);
                        });
                }
            })
            .catch((error) => {
                console.log('Error:' + error);
            });

    });

    // Remove Cart Item
    jQuery(document).on('click', '.remove_cart', function (e) {
        e.preventDefault();
        jQuery(this).closest('.remove_cartitem').remove();
        var productid = jQuery(this).attr('product-id');
        var product_type = jQuery(this).attr('product-type');
        var product_variation_id = jQuery(this).attr('product-variation_id');
        console.log(productid);
        console.log(product_type);
        console.log(product_variation_id);
        const url = '/cart';
        const inputdata = {
            method: "DELETE",
            data: {
                id: productid,
                product_type: product_type,
                variation_id: product_variation_id
            }
        };
        postData(url, inputdata, 'json')
            .then((json) => {
                console.log(json);
                if (json.status) {
                    jQuery('.product_count').empty().text(json.items.length);
                    getData('/cartmodule', 'text')
                        .then((content) => {
                            jQuery('#cartPopupOpen').trigger('click');
                            jQuery('#cartPopup').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('wish module error:' + err);
                        });

                    getData('/cartupdate', 'text')
                        .then((content) => {
                            console.log(content);
                            jQuery('#cartinfo').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('Cart Update Error:' + err);
                        });
                }
            })
            .catch((error) => {
                console.log('Error:' + error);
            });

    });

    // Start Addtocart Script
    jQuery(document).on('click', '.addtocart', function () {

        var productid = parseInt(jQuery(this).attr('product-id'));
        var quantity = parseInt(jQuery(this).attr('product-qty'));
        var variation_id = parseInt(jQuery(this).attr('variation-id'));
        var dt = {};
        if (variation_id > 0) {
            dt = {
                id: productid,
                quantity: quantity,
                variation_id: variation_id,
            }
        } else {
            dt = {
                id: productid,
                quantity: quantity
            }
        }

        const url = '/cart';

        const inputdata = {
            method: 'POST',
            data: dt
        };

        postData(url, inputdata, 'json')
            .then((json) => {
                console.log(json);
                if (json.status) {
                    jQuery('.product_count').empty().text(json.items.length);
                    getData('/cartmodule', 'text')
                        .then((content) => {
                            jQuery('#cartPopupOpen').trigger('click');
                            jQuery('#cartPopup').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('cart module error:' + err);
                        });
                }
            }).catch((error) => {
                console.log('Error:' + error);
            });

    });
    // End Addtocart Script

    jQuery(document).on('change', '.shopqty', function () {
        var selt = jQuery(this).find('option:selected').val();
        jQuery(this).closest('.cart_table').find('.addtocart').attr('product-qty', selt);
    });

    jQuery(document).on('change', '.cartqty', function () {
        jQuery('.updatecart').removeAttr('disabled');
    });

    /*jQuery('.table-responsive').on('click', '.update_cart', function () {
        console.log('Testing');
        jQuery('form#cartform').submit(function (e) {
            e.preventDefault();
            var formdata = jQuery('form#cartform').serialize();
            console.log('testingdsadsafasdf');
            console.log(formdata);
        });
    });*/

    jQuery(document).on('submit', 'form#cartform', function (e) {
        //jQuery('form#cartform').submit(function (e) {
        e.preventDefault();
        jQuery('.btnldr').css({ display: "block" });
        var cartdata = jQuery(this).serialize();
        var cartitems = [];
        jQuery('.cartqty').each(function () {
            cartitems.push({ 'id': parseInt(jQuery(this).attr('product-id')), 'quantity': parseInt(jQuery(this).val()) });
        });
        console.log(cartitems);
        const url = '/cartupdate';
        const inputdata = {
            method: 'POST',
            data: {
                cartdata: cartitems.reverse()
            }
        };
        postData(url, inputdata, 'json')
            .then((json) => {
                if (json.status) {
                    jQuery('.product_count').empty().text(json.items.length);
                    getData('/cartupdate', 'text')
                        .then((content) => {
                            console.log(content);
                            jQuery('#cartinfo').empty().append(content);
                        })
                        .catch((err) => {
                            console.log('Cart Update Error:' + err);
                        });
                }
            })
            .catch((error) => {
                console.log('Error:' + error);
            });

        jQuery('.btnldr').css({ display: "none" });
    });
    jQuery('input[id="versanddetail"]').click(function () {
        console.log(jQuery('#versanddetail').is(':checked'));
        validateShipping();
    });

    function validateShipping() {
        if (jQuery('#versanddetail').is(':checked')) {
            jQuery('.inputbox input').attr('data-valcode', "!b");
            jQuery('.selectbox select').attr('data-valcode', "sel");
        } else {
            jQuery('.inputbox input').attr('data-valcode', "").val("");
            jQuery('.selectbox select').attr('data-valcode', "");
        }
    }

    jQuery(document).on('submit', 'form#registerform', function (e) {

        e.preventDefault();
        jQuery(".process-loader").show();
        var formdata = jQuery(this).serialize();

        const url = '/register';
        const inputdata = {
            method: 'POST',
            data: {
                formdata: formdata
            }
        };

        postData(url, inputdata, 'json')
            .then(json => {
                jQuery(".process-loader").hide();
                console.log(json);
                if (json.status) {
                    document.getElementById("registerform").reset();
                    alertify.confirm(json.message,
                        function () {
                            alertify.success('Thank You');
                            window.location.href = "/login";
                        },
                        function () {
                            alertify.success('Please Continue to Login...');
                        });
                } else {
                    document.getElementById("registerform").reset();
                    alertify.confirm(json.message,
                        function () {
                            alertify.success('Thank You');
                            window.location.href = "/login";
                        },
                        function () {
                            alertify.success('Thank You');
                        });
                    //alertify.error(json.message);
                }
            })
            .catch(error => {
                jQuery(".process-loader").hide();
                console.log(error);
            });

    });

    jQuery(document).on('submit', 'form#loginform', function (e) {
        e.preventDefault();
        jQuery(".process-loader").show();
        var formdata = jQuery(this).serialize();
        const url = '/login';
        const inputdata = {
            method: 'POST',
            data: {
                formdata: formdata
            }
        };

        postData(url, inputdata, 'json')
            .then(json => {
                jQuery(".process-loader").hide();
                if (json.status) {
                    document.getElementById("loginform").reset();
                    alertify.success(json.message);
                    setTimeout(function () {
                        window.location.href = json.redirect;
                    }, 1000);
                } else {
                    alertify.error(json.message);
                }
                console.log(json);
            })
            .catch(error => {
                jQuery(".process-loader").hide();
                console.log(error);
            });
    });

    jQuery(document).on('submit', 'form#checkoutform', function (e) {
        e.preventDefault();
        jQuery(".process-loader").show();
        var formdata = jQuery(this).serialize();
        const url = '/order';
        const inputdata = {
            method: 'POST',
            data: {
                formdata: formdata
            }
        };

        postData(url, inputdata, 'json')
            .then(json => {
                jQuery(".process-loader").hide();
                console.log(json);
                if (json.status) {
                    alertify.success(json.message);
                    setTimeout(function () {
                        window.location.href = "/order-received/" + json.order_id + "/?key=" + json.order_key;
                    }, 1000);
                } else {
                    alertify.error(json.message);
                }
            })
            .catch(error => {
                jQuery(".process-loader").hide();
                console.log(error);
            });
    });

    jQuery(document).on('submit', 'form#reviewform', function (e) {
        e.preventDefault();

        jQuery(".process-loader").show();
        var formdata = jQuery(this).serialize();
        var encodedata = encodeURIComponent(formdata);
        const url = '/review';
        const inputdata = {
            method: 'POST',
            data: {
                formdata: formdata
            }
        };

        postData(url, inputdata, 'json')
            .then(json => {
                jQuery(".process-loader").hide();
                //console.log(json);
                if (json.status) {
                    document.getElementById("reviewform").reset();
                    alertify.success(json.message);
                } else {
                    alertify.error(json.message);
                }
            })
            .catch(error => {
                jQuery(".process-loader").hide();
                console.log(error);
            });
    });

    //reviewForm
    jQuery(document).on('submit', 'form#myaccount', function (e) {
        e.preventDefault();
        jQuery(".process-loader").show();
        var formdata = jQuery(this).serialize();
        /*var arraydata = jQuery(this).serializeArray();
        console.log(arraydata);
        return false;*/

        const url = '/updateaccount';
        const inputdata = {
            method: 'POST',
            data: {
                formdata: formdata
            }
        };

        postData(url, inputdata, 'json')
            .then(json => {
                jQuery(".process-loader").hide();
                //console.log(json);
                if (json.status) {
                    //document.getElementById("myaccount").reset();
                    alertify.success(json.message);
                } else {
                    alertify.error(json.message);
                }
            })
            .catch(error => {
                jQuery(".process-loader").hide();
                console.log(error);
            });
    });

    jQuery(document).on('click',".chkoutlogin", function() {
        jQuery('.loginform').slideToggle("500");
    });

    /*//product Filter
    jQuery(document).on('change', '.azflt, .priceflt, .noflt, .pageflt', function () {
        jQuery('.shopoverlay').show();
        jQuery('.spage').val(1);
        jQuery('.pageloader').attr('data-visible', 1);
        jQuery('#filtersubmit').trigger('click');
    });

    //Product Submit filter
    jQuery(document).on('submit', 'form#productfilter', function (e) {
        e.preventDefault();
        var formdata = jQuery(this).serialize();
        var page = parseInt(jQuery('.spage').val());
        const url = '/productupdate';
        jQuery.get(url, formdata, function (res) {
            console.log(typeof (res));
            jQuery('.pageloader').hide();
            jQuery('.shopoverlay').hide();
            if (res.hasOwnProperty('status') && typeof (res) == 'object') {
                console.log('test success');
                jQuery('.pageloader').attr('data-visible', 0);
            } else {
                if (page > 1) {
                    console.log('Many');
                    jQuery('.shop_products').append(res).hide().fadeIn(1000);
                } else {
                    console.log('One');
                    jQuery('.shop_products').empty().append(res).hide().fadeIn(1000);
                }
            }
            lazyload();
        });
        jQuery("#loadmore").removeAttr('disabled');
        return false;
    });

    jQuery(document).on('click', '#loadmore', function (e) {
        e.preventDefault();
        var page = parseInt(jQuery('.spage').val());
        page++;
        jQuery('.spage').val(page);
        jQuery(this).val(page).attr('disabled', 'disabled');
        setTimeout(function() {
            jQuery('#filtersubmit').trigger('click');
        }, 500);
        console.log(page);
    });

    jQuery(window).scroll(function () {
        var winScrlTop = parseInt(jQuery(window).scrollTop());
        var cnt = jQuery('.shop_products');
        var isLoadEnable = parseInt(jQuery('.pageloader').attr('data-visible'));
        var getFinalHeight = parseInt(cnt.height()/2 + cnt.offset().top);
        console.log("scroll Top: " + winScrlTop);
        console.log("Shop Height: " + parseInt(cnt.height()));
        console.log("Final Height: " + getFinalHeight);

        if (parseInt(winScrlTop) >= getFinalHeight) {
            console.log('end Reached');
            var page = parseInt(jQuery('.spage').val());
            page++;
            jQuery('.spage').val(page);
            //jQuery(this).val(page).attr('disabled', 'disabled');
            if (page > 1 && isLoadEnable) {
                console.log(isLoadEnable);
                jQuery('.pageloader').show();
                jQuery('#filtersubmit').trigger('click');
            }
            console.log(page);
        }

    });*/

});