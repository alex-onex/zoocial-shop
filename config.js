var mysql = require('mysql');

var config = {
	db: {
		host: '192.168.1.134', 	// database host
		user: 'root', 		// your database username
		password: "", 		// your database password
		port: 3306, 		// default MySQL port
		db: 'at_zoocialshop', 		// your database name
		charset: 'utf8_general_ci' // charset
	},
	server: {
		host: '127.0.0.1',
		port: '4005'
	},
	wcsettings: {
		url: "https://zoocialshop.ch/",
   		consumerKey: "ck_4e95f3bc2e84c73630e6b9065950dc53cc8f7f9e",
   		consumerSecret: "cs_2e41e4f3db2ef3ed118ba8254c483922488e1196",
   		version: "wc/v3"
	},
	/*wcsettings: {
		url: "http://192.168.1.134/athavullah/wordpress/project1/",
   		consumerKey: "ck_94e2539e4f538474ba407e58593e0c7bb59cca37",
   		consumerSecret: "cs_dd0e503faf984fe237f376c0d883188e1095c341",
   		version: "wc/v3"
	}*/
}

module.exports = config;