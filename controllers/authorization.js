
var mAuthorization = require("../models/authorization");

Authorization = {

    async register(req, res) {

        console.log(req.method);

        if (req.method.toLowerCase() === 'get') {
            var result = await mAuthorization.register(req);
            res.render("shop/pages/register", result);
        }

        if (req.method.toLowerCase() === 'post') {
            res.setHeader('Content-Type', 'application/json');
            var result = await mAuthorization.create(req);
            res.send(result);
        }

        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'register', 'items': req.session.cartitems }));
    },
    async login(req, res) {

        console.log(req.method);

        if (req.method.toLowerCase() === 'get') {
            var result = await mAuthorization.login(req);
            res.render("shop/pages/login", result);
        }

        if (req.method.toLowerCase() === 'post') {
            res.setHeader('Content-Type', 'application/json');
            var result = await mAuthorization.authorize(req);
            var citems = req.session.cartitems;
            result.redirect = (citems.length > 0) ? "/checkout": "/";;
            res.send(result);
        }

        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'register', 'items': req.session.cartitems }));
    },
    async isAuthorize(req, res, next) {
        if (req.session.auth.hasOwnProperty('id')) {
            next();
        } else {
            res.redirect("/");
        }
    },
    async authChecker(req, res, next) {
        if (req.session.auth.hasOwnProperty('id')) {
            req.flash('notify', 'You Have Been Already Login, Please Continue shopping...');
            res.redirect("/");
        } else {
            next();
        }
    },
    async logout(req, res) {
        req.session.auth = {};
        req.flash('notify', 'Logout Successfully');
        res.redirect("/");
    },
    async myAccount(req, res) {
        var result = await mAuthorization.myAccount(req);
        res.render("shop/pages/myaccount", result);
    },
    async updateAccount(req, res) {
        var result = await mAuthorization.updateAccount(req);
        res.send(result);
    },
    async userMenus(req, res) {
        var result = await mAuthorization.userMenus(req);
        res.render("shop/modules/usermenus", result);
    }
}

module.exports = Authorization;