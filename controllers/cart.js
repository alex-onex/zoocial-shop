
var mCart = require("../models/cart");

Cart = {
    async create(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mCart.create(req);
        res.send(result);
    },
    async delete(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mCart.delete(req);
        res.send(result);
    },
    async lists(req, res) {
        var citems = req.session.cartitems;
        var result = await mCart.cart(req, true);
        res.render("shop/pages/cart", result);
        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'lists', 'items': req.session.cartitems }));
    },
    async module(req, res) {
        var citems = req.session.cartitems;
        if (citems.length > 0 && typeof citems != 'undefined') {
            var result = await mCart.cart(req);
            res.render("shop/modules/minicart", result);
        } else {
            var str = `<div class="form-header">
                       <h4>Lieblingsartikel</h4>
                       <div id="cartPopupClose" class="slideFormClose">×</div>
                        </div>
                        <h3>Keine Favoriten hinzugefügt ...</h3>
                        `;
            res.send(str);
        }
    },
    async update(req, res) {

        console.log(req.method);

        if (req.method.toLowerCase() === 'get') {
            var result = await mCart.cart(req);
            res.render("shop/modules/cartupdate", result);
        }

        if (req.method.toLowerCase() === 'post') {
            req.session.cartitems = [];
            req.session.cartitems = req.body.cartdata;
            res.setHeader('Content-Type', 'application/json');
            var result = { "status": 200, "method": 'update', 'items': req.session.cartitems };
            res.send(result);
        }

    }
}

module.exports = Cart;