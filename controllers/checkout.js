
var mCheckout = require("../models/checkout");
var mCart = require("../models/cart");

Checkout = {

    async checkout(req, res) {
        var isCartAvail = await mCart.isCartAvail(req);
        var cart = await mCart.cart(req);
        //console.log(isCartAvail);
        //console.log(cart);
        if(isCartAvail) {
            var result = await mCheckout.checkout(req);
            result.cart = cart;
            res.render("shop/pages/checkout", result);
        } else {
            res.redirect('/cart');
        }
    },
    async order(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mCheckout.createorder(req);
        res.send(result);
    }

}

module.exports = Checkout;