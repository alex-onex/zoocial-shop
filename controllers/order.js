
var mOrder = require("../models/order");

Order = {
    
    async orderSuccess(req, res) {
        //res.setHeader('Content-Type', 'application/json');
        var result = await mOrder.orderSuccess(req);
        console.log(result);
        res.render("shop/pages/order-received", result);
        //res.send(result);
    },
    async viewOrder(req, res) {
        //res.setHeader('Content-Type', 'application/json');
        var result = await mOrder.orderSuccess(req);
        console.log(result);
        res.render("shop/pages/view-order", result);
        //res.send(result);
    },
    async lists(req, res) {
        //res.setHeader('Content-Type', 'application/json');
        var result = await mOrder.lists(req);
        console.log(result);
        res.render("shop/pages/myorders", result);
        //res.send(result);
    }

}

module.exports = Order;