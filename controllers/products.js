const mProducts = require('../models/products');

Products = {
    async create(req, res) {
        var result = await mProducts.create(req);
        res.send(result);
    },
    async update(req, res) {
        var result = await mProducts.update(req);
        res.send(result);
    },
    async delete(req, res) {
        var result = await mProducts.delete(req);
        res.send(result);
    },
    async lists(req, res) {
        var result = await mProducts.lists(req);
        res.render("shop/pages/shop", result);
    },
    async list(req, res) {
        var result = await mProducts.list(req);
        res.render("shop/pages/singleproduct", result);
    },
    async singleProduct(req, res) {
        var result = await mProducts.singleProduct(req);
        res.render("shop/pages/singleproduct", result);
    },
    async tags(req, res) {
        var result = await mProducts.tags(req);
        res.render("shop/pages/brand", result);
    },
    async createReview(req, res) {
        var result = await mProducts.createReview(req);
        res.send(result);
    },
    async filter(req, res) {
        var result = await mProducts.filter(req);
        if (result.status) {
            res.render("shop/modules/productupdate", result);
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.send(result);
        }

    },
    async homePageNew(req, res) {
        var result = await mProducts.homePageNew(req);
        res.render("shop/pages/homepage", result);
    }
}

module.exports = Products;