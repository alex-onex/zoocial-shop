var mWishlist = require("../models/wishlist");

Wishlist = {
    async create(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mWishlist.create(req);
        res.send(result);
    },
    async delete(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mWishlist.delete(req);
        res.send(result);
    },
    async lists(req, res) {
        var result = await mWishlist.lists(req);
        res.render("shop/pages/wishlist", data);
    },
    async list(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var result = await mWishlist.list(req);
        res.send(result);
    },
    async module(req, res) {
        //res.setHeader('Content-Type', 'application/json');

        let favitems = req.session.favitems;

        if (favitems.length > 0 && typeof favitems != 'undefined') {
            var result = await mWishlist.module(req);
            res.render("shop/modules/wishlist", result);
        } else {
            var result = `<div class="form-header">
                       <h4>Lieblingsartikel</h4>
                       <div id="favClose" class="slideFormClose">×</div>
                        </div>
                        <h3>Keine Favoriten hinzugefügt ...</h3>
                        `;
            res.send(result);
        }
    }
}

module.exports = Wishlist;