
Authorization = {
    async create(req) {

        var formdata = utls.parseQuery(req.body.formdata);

        var personal_info = {
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            company: formdata.company,
            address_1: formdata.address_1,
            address_2: formdata.address_2,
            city: formdata.city,
            state: formdata.state,
            postcode: formdata.postcode,
            country: "CH",
            email: formdata.email,
            phone: formdata.phone
        }

        var ship1 = {
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            company: formdata.company,
            address_1: formdata.address_1,
            address_2: formdata.address_2,
            city: formdata.city,
            state: formdata.state,
            postcode: formdata.postcode,
            country: "CH"
        };
        var ship2 = {
            first_name: formdata.b2_first_name,
            last_name: formdata.b2_last_name,
            company: formdata.b2_company,
            address_1: formdata.b2_address_1,
            address_2: formdata.b2_address_2,
            city: formdata.b2_city,
            state: formdata.b2_state,
            postcode: formdata.b2_postcode,
            country: "CH"
        };

        const data = {
            email: formdata.email,
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            username: formdata.username,
            password: formdata.password,
            billing: personal_info,
            shipping: (formdata.diffshipping && formdata.diffshipping !== 'undefined') ? ship2 : ship1
        };

        let customers = await postWCApiAsync("customers", data);

        if (customers.hasOwnProperty('message')) {
            return { "status": false, "code": customers.data.status, "error": customers.code, "message": customers.message };
        }

        return { "status": true, "code": 200, "error": null, "message": "Your Account is Created, Please Continue to login..." };
    },
    async register(req) {

        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }

        return data;
        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'lists', 'items': req.session.cartitems }));
    },
    async login(req) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }

        return data;
    },
    async authorize(req) {
        var formdata = utls.parseQuery(req.body.formdata);
        var userinput = {};
        var userinfo = {};
        console.log(formdata);

        const url = await config.wcsettings.url + "wp-json/jwt-auth/v1/token";

        const inputData = {
            method: 'POST',
            data: formdata
        };

        let jwtres = await utls.postData(url, inputData);
        console.log(jwtres);
        if (jwtres.hasOwnProperty('code')) {
            if (jwtres.code == '[jwt_auth] invalid_username' || jwtres.code == '[jwt_auth] invalid_email') {
                return {
                    "status": false,
                    "error": true,
                    "message": jwtres.message
                };
            }
            if (jwtres.code == '[jwt_auth] incorrect_password') {
                return {
                    "status": false,
                    "error": true,
                    "message": "Ihr eingegebenes Passwort ist falsch"
                }
            }
        }

        if (jwtres.user_email) {
            userinput = {
                email: jwtres.user_email
            }
            userdata = await getWCApiAsync("customers", userinput);
            userinfo = {
                id: userdata[0].id,
                username: userdata[0].username,
                email: userdata[0].email,
                role: userdata[0].role,
                displayname: jwtres.user_display_name,
                billing: userdata[0].billing,
                shipping: userdata[0].shipping
            }
            //req.session.loguser = userinfo;
            req.session.auth = userinfo;
        }

        return { "status": true, "error": null, "message": "Sucessfully Login" };
    },
    async myAccount(req) {

        let categories = await getWCApiAsync("products/categories");

        data = {
            categories: utls.getNestedChildren(categories, 0),
            userdata: req.session.auth,
        }
        return data;
    },
    async updateAccount(req) {

        var userdata = req.session.auth;
        var formdata = utls.parseQuery(req.body.formdata);
        var data = {};

        var billing = {
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            company: formdata.company,
            address_1: formdata.address_1,
            address_2: formdata.address_2,
            city: formdata.city,
            state: formdata.state,
            postcode: formdata.postcode,
            country: "CH",
            email: formdata.email,
            phone: formdata.phone
        }

        var shipping = {
            first_name: formdata.b2_first_name,
            last_name: formdata.b2_last_name,
            company: formdata.b2_company,
            address_1: formdata.b2_address_1,
            address_2: formdata.b2_address_2,
            city: formdata.b2_city,
            state: formdata.b2_state,
            postcode: formdata.b2_postcode,
            country: "CH"
        };

        const rqdata = {
            email: formdata.email,
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            billing: billing,
            shipping: shipping
        };

        //console.log(rqdata);
        //return false;

        if (userdata.hasOwnProperty('id')) {

            let customer = await putWCApiAsync("customers/" + userdata.id, rqdata);

            userdata.id = customer.id;
            userdata.username = customer.username;
            userdata.email = customer.email;
            userdata.role = customer.role;
            userdata.displayname = customer.username;
            userdata.billing = customer.billing;
            userdata.shipping = customer.shipping;

            data = {
                status: true,
                customer: customer,
                message: "Account Information Update Successfully",
            }

        } else {
            data = {
                status: false,
                customer: customer,
                message: "Sorry!, Something Went Wrong...",
            }
        }
        return data;
    },
    async userMenus(req) {
        var userdata = req.session.auth;
        var data = {};
        if (userdata.hasOwnProperty('id')) {
            data = { "status": true, "user": userdata };
        } else {
            data = { "status": false };
        }
        return data;
    }
}

module.exports = Authorization;