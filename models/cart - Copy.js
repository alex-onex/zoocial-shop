
Cart = {
    create(req, res) {

        res.setHeader('Content-Type', 'application/json');
        let pid = parseInt(req.body.id);
        let qty = parseInt(req.body.quantity);
        let citems = req.session.cartitems;
        let ckavail = false;

        citems.forEach((cartitem, el) => {
            //var quantity = citems[el].quantity;
            if (parseInt(cartitem.id) == pid) {
                ckavail = true;
                req.session.cartitems[el].quantity += parseInt(qty);
                console.log(req.session.cartitems[el]);
            }
        });

        cart = { id: pid, quantity: qty };

        if (!ckavail) {
            req.session.cartitems.push(cart);
        }

        console.log(ckavail);
        console.log(req.session.cartitems.length);
        console.log(req.session.cartitems);

        res.send(JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems }));
    },
    delete(req, res) {

        res.setHeader('Content-Type', 'application/json');

        var cartitems = req.session.cartitems;
        let pid = req.body.id;
        console.log(pid);
        req.session.cartitems = cartitems.filter(cartitem => {
            return cartitem.id != pid;
        });

        console.log(req.session.cartitems);

        res.send(JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.cartitems }));
    },
    async lists(req, res) {

        var citems = req.session.cartitems;
        var cartitems = [];
        var carttotals = 0;

        console.log(citems);

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);
            let categories = await getWCApiAsync("products/categories");

            cartproducts.forEach((cartproduct, el) => {

                var quantity = (cartproduct.id == citems[el].id) ? parseInt(citems[el].quantity) : 0;

                var product = {
                    id: cartproduct.id,
                    thumbnail: cartproduct.images[0].src,
                    name: cartproduct.name,
                    slug: cartproduct.slug,
                    price: parseInt(cartproduct.price),
                    weight: cartproduct.weight,
                    subtotal: parseInt(cartproduct.price) * quantity,
                    quantity: quantity
                }

                cartitems.push(product);
                carttotals += parseInt(product.subtotal);

            });

            console.log(cartitems);
            console.log(carttotals);

            data = {
                cartproducts: cartitems,
                carttotals: carttotals,
                categories: utls.getNestedChildren(categories, 0)
            }

            res.render("shop/pages/cart", data);

        }
        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'lists', 'items': req.session.cartitems }));
    },
    async module(req, res) {
        var citems = req.session.cartitems;
        var cproducts = [];
        var ctotals = 0;

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);
            let categories = await getWCApiAsync("products/categories");

            //console.log(citems);
            //console.log(cartproducts);

            var mergecarts = _.merge(_.keyBy(citems, 'id'), _.keyBy(cartproducts, 'id'));
            var mergeitems = _.values(mergecarts);

            //console.log(mergeitems);

            mergeitems.forEach((mitem, el) => {
                mitem.subtotal = parseInt(mitem.quantity) * parseInt(mitem.price);
                ctotals += parseInt(mitem.quantity) * parseInt(mitem.price);
                cproducts.push(mitem);
            });

            //console.log(cproducts);
            //console.log(ctotals);

            /*cartproducts.forEach((cartproduct, el) => {

                var quantity = (cartproduct.id == citems[el].id) ? parseInt(citems[el].quantity) : 0;

                var product = {
                    id: cartproduct.id,
                    thumbnail: cartproduct.images[0].src,
                    name: cartproduct.name,
                    slug: cartproduct.slug,
                    price: parseInt(cartproduct.price),
                    weight: cartproduct.weight,
                    subtotal: parseInt(cartproduct.price) * quantity,
                    quantity: quantity
                }

                cartitems.push(product);
                carttotals += parseInt(product.subtotal);

            });*/

            data = {
                cartproducts: cproducts,
                carttotals: ctotals
            }

            res.render("shop/modules/minicart", data);

        } else {
            var str = `<div class="form-header">
                       <h4>Lieblingsartikel</h4>
                       <div id="cartPopupClose" class="slideFormClose">×</div>
                        </div>
                        <h3>Keine Favoriten hinzugefügt ...</h3>
                        `;
            res.send(str);
        }
    }
}

module.exports = Cart;