
Cart = {
    create(req) {

        /*console.log(req.body);
        return false;*/

        //res.setHeader('Content-Type', 'application/json');
        let pid = parseInt(req.body.id);
        let qty = parseInt(req.body.quantity);
        let vid = parseInt(req.body.variation_id);
        let citems = req.session.cartitems;
        let ckavail = false;
        let availvarid = false;

        citems.forEach((cartitem, el) => {
            //var quantity = citems[el].quantity;
            if (parseInt(cartitem.id) == pid) {
                ckavail = true;
                if (!cartitem.hasOwnProperty('variation_id')) {
                    req.session.cartitems[el].quantity += parseInt(qty);
                } else {
                    if (cartitem.variation_id == vid) {
                        req.session.cartitems[el].quantity += parseInt(qty);
                        availvarid = true;
                    }
                }
                //console.log(req.session.cartitems[el]);
            }
        });

        //cart = { id: pid, quantity: qty };
        //cart = { id: pid, quantity: qty, variation_id: vid };
        cart = (req.body.hasOwnProperty('id')) ? req.body : {};

        if (!ckavail || !availvarid) {
            req.session.cartitems.push(cart);
        }

        var data = JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems });
        return data;

        //res.send(JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems }));
    },
    delete(req) {

        //res.setHeader('Content-Type', 'application/json');

        var cartitems = req.session.cartitems;
        let pid = req.body.id;
        let variation_id = req.body.variation_id;
        let ptype = req.body.product_type;
        console.log(pid);
        console.log(variation_id);
        console.log(ptype);
        req.session.cartitems = cartitems.filter(cartitem => {
            if (ptype == 'variable' && variation_id > 0) {
                return cartitem.variation_id != variation_id;
            } else {
                return cartitem.id != pid;
            }
        });

        console.log(req.session.cartitems);

        var data = JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.cartitems });

        return data;

        //res.send(JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.cartitems }));
    },
    async lists(req) {

        var citems = req.session.cartitems;
        //var cartitems = [];
        var carttotals = 0;
        var pitems = [];
        var itms = [];

        //console.log(citems);
        let categories = await getWCApiAsync("products/categories");

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);

            var results = utls.joinObjects(citems, cartproducts);
            var st = 0;
            results.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                st += parseInt(result.quantity) * parseFloat(result.price);
                pitems.push(result);
            });

            //console.log(pitems);
            //console.log(st);

            data = {
                cartproducts: pitems,
                carttotals: st,
                categories: utls.getNestedChildren(categories, 0)
            }

        } else {
            data = {
                cartproducts: [],
                carttotals: 0,
                categories: utls.getNestedChildren(categories, 0)
            }
        }
        return data;
        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'lists', 'items': req.session.cartitems }));
    },
    async cart(req, cats = false) {
        var citems = req.session.cartitems;
        var cartitems = [];
        var cartvitems = [];
        var carttotal = 0;
        var pitems = [];
        var discount = 0;

        //console.log(citems);
        if (cats) {
            var categories = await getWCApiAsync("products/categories");
        }

        if (citems.length > 0 && typeof (citems) != 'undefined') {

            // return non variation objects
            var cdata = citems.filter(function (itm) {
                return !itm.hasOwnProperty('variation_id');
            });

            // return variation objects
            var cvdata = citems.filter(function (itm) {
                return itm.hasOwnProperty('variation_id');
            });

            //return only cdata IDS
            var cids = cdata.map((citem) => {
                return citem.id;
            });

            //console.log(cids.length);

            inputdata = {
                include: cids
            }

            if (cids.length > 0) {
                let cproducts = await getWCApiAsync("products", inputdata);
                var cartitems = await utls.joinObjects(cdata, cproducts);
            }

            var minqty = {};
            var disPercentage = {};
            var discount_quantity = null;
            var discount_percentage = null;
            for (const file of cvdata) {
                //console.log(file.quantity);
                var product = await getWCApiAsync("products/" + parseInt(file.id));
                var variation = await getWCApiAsync("products/" + parseInt(file.id) + "/variations/" + parseInt(file.variation_id));
                //console.log(variation);
                variation.meta_data.forEach((metainfo, el) => {
                    if (metainfo.key == "discount_quantity") {
                        discount_quantity = metainfo;
                    }
                    if (metainfo.key == "discount_percentage") {
                        discount_percentage = metainfo;
                    }
                });
                //console.log(file.quantity);
                //console.log(parseInt(minqty.value));
                //console.log(parseFloat(disPercentage.value));
                product.variation_id = file.variation_id;
                product.quantity = file.quantity;
                product.sku = variation.sku;
                product.price = (parseFloat(discount_percentage.value) > 0 && parseInt(file.quantity) >= parseInt(discount_quantity.value)) ? await utls.discountPrice(variation.price, discount_percentage.value) : variation.price;
                /*if (parseFloat(disPercentage.value) > 0 && parseInt(file.quantity) >= parseInt(minqty.value)) {
                    console.log('11111');
                    product.price = await utls.discountPrice(variation.price, disPercentage.value);
                } else {
                    console.log('22222');
                    product.price = variation.price;
                }*/
                //product.price = variation.price;
                product.regular_price = variation.regular_price;
                product.sale_price = variation.sale_price;
                product.attributes = variation.attributes;
                product.images = product.images;
                cartvitems.push(product);
            }

            //console.log(cartvitems);

            var cartfinal = [...cartitems, ...cartvitems];

            cartfinal.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                carttotal += parseInt(result.quantity) * parseFloat(result.price);
                discount += parseInt(result.quantity) * (parseFloat(result.regular_price) - parseFloat(result.price));
                pitems.push(result);
            });

            var data = {
                cartproducts: pitems,
                carttotal: carttotal,
                discount: discount,
                categories: (cats) ? utls.getNestedChildren(categories, 0) : null
            }

        } else {
            var data = {
                cartproducts: null,
                carttotal: null,
                discount: null,
                categories: (cats) ? utls.getNestedChildren(categories, 0) : null
            }
        }

        return data;
    },
    async isCartAvail(req) {
        var citems = req.session.cartitems;
        return (parseInt(citems.length) > 0) ? true : false;
    },
}

module.exports = Cart;