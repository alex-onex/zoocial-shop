
Cart = {
    create(req) {

        /*console.log(req.body);
        return false;*/

        //res.setHeader('Content-Type', 'application/json');
        let pid = parseInt(req.body.id);
        let qty = parseInt(req.body.quantity);
        let vid = parseInt(req.body.variation_id);
        let citems = req.session.cartitems;
        let ckavail = false;
        let availvarid = false;

        citems.forEach((cartitem, el) => {
            //var quantity = citems[el].quantity;
            if (parseInt(cartitem.id) == pid) {
                ckavail = true;
                if (!cartitem.hasOwnProperty('variation_id')) {
                    req.session.cartitems[el].quantity += parseInt(qty);
                } else {
                    if (cartitem.variation_id == vid) {
                        req.session.cartitems[el].quantity += parseInt(qty);
                        availvarid = true;
                    }
                }
                //console.log(req.session.cartitems[el]);
            }
        });

        //cart = { id: pid, quantity: qty };
        //cart = { id: pid, quantity: qty, variation_id: vid };
        cart = (req.body.hasOwnProperty('id')) ? req.body : {};

        if (!ckavail || !availvarid) {
            req.session.cartitems.push(cart);
        }

        var data = JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems });
        return data;

        //res.send(JSON.stringify({ "status": 200, "method": 'create', 'items': req.session.cartitems }));
    },
    delete(req) {

        //res.setHeader('Content-Type', 'application/json');

        var cartitems = req.session.cartitems;
        let pid = req.body.id;
        let variation_id = req.body.variation_id;
        let ptype = req.body.product_type;
        console.log(pid);
        console.log(variation_id);
        console.log(ptype);
        req.session.cartitems = cartitems.filter(cartitem => {
            if(ptype == 'variable' && variation_id > 0) {
                return cartitem.variation_id != variation_id;
            } else {
                return cartitem.id != pid;
            }
        });

        console.log(req.session.cartitems);

        var data = JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.cartitems });

        return data;

        //res.send(JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.cartitems }));
    },
    async lists(req) {

        var citems = req.session.cartitems;
        //var cartitems = [];
        var carttotals = 0;
        var pitems = [];
        var itms = [];

        //console.log(citems);
        let categories = await getWCApiAsync("products/categories");

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);

            var results = utls.joinObjects(citems, cartproducts);
            var st = 0;
            results.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                st += parseInt(result.quantity) * parseFloat(result.price);
                pitems.push(result);
            });

            //console.log(pitems);
            //console.log(st);

            data = {
                cartproducts: pitems,
                carttotals: st,
                categories: utls.getNestedChildren(categories, 0)
            }

        } else {
            data = {
                cartproducts: [],
                carttotals: 0,
                categories: utls.getNestedChildren(categories, 0)
            }
        }
        return data;
        //res.setHeader('Content-Type', 'application/json');
        //res.send(JSON.stringify({ "status": 200, "method": 'lists', 'items': req.session.cartitems }));
    },
    async module(req) {
        var citems = req.session.cartitems;
        //var cartitems = [];
        var carttotals = 0;
        var pitems = [];
        var discount = 0;
        var itms = [];
        var ts = 0,
            dis = 0;

        /*for (const file of citems) {
            var itm = {};
            var product = await getWCApiAsync("products/" + parseInt(file.id));
            itm.id = product.id;
            itm.variation_id = (file.hasOwnProperty('variation_id')) ? file.variation_id : 0;
            itm.quantity = file.quantity;
            itm.name = product.name;
            itm.slug = product.slug;
            itm.type = product.type;
            itm.status = product.status;
            itm.sku = product.sku;
            itm.price = product.price;
            itm.regular_price = product.regular_price;
            itm.sale_price = product.sale_price;
            itm.on_sale = product.on_sale;
            itm.attributes = product.attributes;
            itm.images = product.images;
            itm.subtotal = parseInt(file.quantity) * parseFloat(product.price);
            ts += (!file.hasOwnProperty('variation_id')) ? parseInt(file.quantity) * parseFloat(product.price) : 0;
            dis += (!file.hasOwnProperty('variation_id')) ? parseInt(file.quantity) * (parseFloat(product.regular_price) - parseFloat(product.price)) : 0;

            if (file.hasOwnProperty('variation_id')) {
                var variation = await getWCApiAsync("products/" + parseInt(file.id) + "/variations/" + parseInt(file.variation_id));
                itm.sku = variation.sku;
                itm.price = variation.price;
                itm.regular_price = variation.regular_price;
                itm.sale_price = variation.sale_price;
                itm.attributes = variation.attributes;
                itm.subtotal = parseInt(file.quantity) * parseFloat(variation.price);
                ts += (file.hasOwnProperty('variation_id')) ? parseInt(file.quantity) * parseFloat(variation.price) : 0;
                dis += (file.hasOwnProperty('variation_id')) ? parseInt(file.quantity) * (parseFloat(variation.regular_price) - parseFloat(variation.price)) : 0;
            }

            itms.push(itm);
        }*/

        //console.log(itms);
        //console.log(ts);
        //console.log(dis);

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);

            var results = utls.joinObjects(citems, cartproducts);
            var st = 0;
            var pt = 0;
            results.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                st += parseInt(result.quantity) * parseFloat(result.price);
                discount += parseInt(result.quantity) * (parseFloat(result.regular_price) - parseFloat(result.price));
                //pt += parseInt(result.quantity) * parseFloat(result.regular_price);
                pitems.push(result);
            });

            //console.log(pitems);
            //console.log(discount);
            //console.log(st);

            var data = {
                cartproducts: pitems,
                carttotals: st
            }

            //res.render("shop/modules/minicart", data);

        } else {
            var data = `<div class="form-header">
                       <h4>Lieblingsartikel</h4>
                       <div id="cartPopupClose" class="slideFormClose">×</div>
                        </div>
                        <h3>Keine Favoriten hinzugefügt ...</h3>
                        `;
            //res.send(data);
        }

        return data;
    },
    async dmodule(req) {
        var citems = req.session.cartitems;
        var cartitems = [];
        var cartvitems = [];
        var carttotal = 0;
        var pitems = [];
        var discount = 0;

        if (citems.length > 0 && typeof(citems) != 'undefined') {

            // return non variation objects
            var cdata = citems.filter(function (itm) {
                return !itm.hasOwnProperty('variation_id');
            });

            // return variation objects
            var cvdata = citems.filter(function (itm) {
                return itm.hasOwnProperty('variation_id');
            });

            //return only cdata IDS
            var cids = cdata.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cproducts = await getWCApiAsync("products", inputdata);
            var cartitems = await utls.joinObjects(cdata, cproducts);
            
            for (const file of cvdata) {
                var product = await getWCApiAsync("products/" + parseInt(file.id));
                var variation = await getWCApiAsync("products/" + parseInt(file.id) + "/variations/" + parseInt(file.variation_id));
                product.variation_id = file.variation_id;
                product.quantity = file.quantity;
                product.sku = variation.sku;
                product.price = variation.price;
                product.regular_price = variation.regular_price;
                product.sale_price = variation.sale_price;
                product.attributes = variation.attributes;
                product.images = product.images;
                cartvitems.push(product);
            }

            var cartfinal = [...cartitems, ...cartvitems];

            cartfinal.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                carttotal += parseInt(result.quantity) * parseFloat(result.price);
                discount += parseInt(result.quantity) * (parseFloat(result.regular_price) - parseFloat(result.price));
                pitems.push(result);
            });

            var data = {
                cartproducts: pitems,
                carttotals: carttotal,
                discount: discount
            }

        } else {
            var data = {
                cartproducts: null,
                carttotals: null,
                discount: null
            }
        }

        return data;
    },
    async isCartAvail(req) {
        var citems = req.session.cartitems;
        return (parseInt(citems.length) > 0) ? true : false;
    },
    async cartTotal(req) {
        var citems = req.session.cartitems;
        var st = 0;
        var pitems = [];
        var data = {};
        var discount = 0;

        if (citems.length > 0 && typeof citems != 'undefined') {

            //return only IDS
            var cids = citems.map((citem) => {
                return citem.id;
            });

            inputdata = {
                include: cids
            }

            let cartproducts = await getWCApiAsync("products", inputdata);
            var results = utls.joinObjects(citems, cartproducts);
            var st = 0;
            results.forEach(result => {
                result.subtotal = parseInt(result.quantity) * parseFloat(result.price);
                st += parseInt(result.quantity) * parseFloat(result.price);
                discount += parseInt(result.quantity) * (parseFloat(result.regular_price) - parseFloat(result.price));
                pitems.push(result);
            });

            data = {
                cproducts: pitems,
                total: st,
                discount: discount
            }
        }
        return data;
    }
}

module.exports = Cart;