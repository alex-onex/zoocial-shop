

Categories = {
    create(req) {
        var data = JSON.stringify({ "status": true, "method": 'create' });
        return data;
    },
    update(req) {
        var data = JSON.stringify({ "status": true, "method": 'update' });
        return data;
    },
    delete(req) {
        var data = JSON.stringify({ "status": true, "method": 'delete' });
        return data;
    },
    async lists(req, res) {

        let reqdata = {
            status: 'publish',
            per_page: 9,
            page: 1,
            orderby: "date",
            order: "desc"
        }

        let products = await getWCApiAsync("products", reqdata);
        let categories = await getWCApiAsync("products/categories");
        let tags = await getWCApiAsync("products/tags");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0),
            tags: tags,
        }

        return data;

        //res.render("shop/pages/categories", data);

    },

    async list(req, res) {

        let rqdata = {
            status: 'publish',
            per_page: 9,
            category: req.params.id,
            page: 1,
            orderby: "date",
            order: "desc"
        }

        let products = await getWCApiAsync("products", rqdata);
        let categories = await getWCApiAsync("products/categories");
        let tags = await getWCApiAsync("products/tags");

        var getSubcategories = categories.filter(function (category) {
            return category.parent == req.params.id;
        });

        //console.log(getSubcategories);

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0),
            subcategories: (getSubcategories.length > 0) ? getSubcategories : null,
            tags: tags,
            category_id: rqdata.category,
            customeScript: true,
        }
        
        return data;

        //res.render('shop/pages/category', data);
    },

    async singleCategory(req, res) {


        let rqdata = {
            status: 'publish',
            per_page: 9,
            category: req.params.id,
            page: 1,
            orderby: "date",
            order: "desc"
        }

        let products = await getWCApiAsync("products", rqdata);
        let categories = await getWCApiAsync("products/categories");
        let singleCategory = await getWCApiAsync("products/categories/"+req.params.id);
        let tags = await getWCApiAsync("products/tags");
        //let pld = await utls.getNestedChildren(categories, 0);
        
        //console.log(pld);
        console.log(singleCategory.parent);

        if(singleCategory.parent) {
            var listSubCategories = categories.filter(function (category) {
                return category.parent == singleCategory.parent;
            });
        } else {
            var listSubCategories = categories.filter(function (category) {
                return category.parent == req.params.id;
            });
        }
        
        /*var getSubcategories = categories.filter(function (category) {
            return category.parent == req.params.id;
        });*/


        //console.log(categories);
        //console.log(getSubcategories);
        //console.log(singleCategory);

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0),
            //subcategories: (getSubcategories.length > 0) ? getSubcategories : null,
            listSubCategories: (listSubCategories.length > 0) ? listSubCategories : null,
            tags: tags,
            category_id: rqdata.category,
            customeScript: {enable: true}
        }

        //console.log(data.subcategories);

        return data;

        //res.render('shop/pages/category', data);
    }
}

module.exports = Categories;