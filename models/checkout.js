
Checkout = {
    async checkout(req) {

        let categories = await getWCApiAsync("products/categories");
        //let shipping_methods = await getWCApiAsync("shipping/zones/5/methods");
        let shipping_methods = await getWCApiAsync("shipping_methods");
        let payment_methods = await getWCApiAsync("payment_gateways");

        data = {
            categories: utls.getNestedChildren(categories, 0),
            userdata: (req.session.auth.hasOwnProperty('id')) ? req.session.auth : 0,
            shipmethods: shipping_methods,
            paymethods: payment_methods
        }

        return data;
    },
    async createorder(req) {

        console.log(req.method);

        var citems = req.session.cartitems;
        var userdata = req.session.auth;
        var isUserAvail = Object.keys(userdata).length;

        var shopitems = [];
        citems.forEach(citem => {
            var ls = {};
            ls.product_id = citem.id;
            ls.quantity = citem.quantity;
            if (citem.hasOwnProperty('variation_id')) {
                ls.variation_id = citem.variation_id;
            }
            shopitems.push(ls);
        });

        var formdata = utls.parseQuery(req.body.formdata);

        var billing = {
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            company: formdata.company,
            address_1: formdata.address_1,
            address_2: formdata.address_2,
            city: formdata.city,
            state: formdata.state,
            postcode: formdata.postcode,
            country: "CH",
            email: formdata.email,
            phone: formdata.phone
        }

        var shipping1 = {
            first_name: formdata.first_name,
            last_name: formdata.last_name,
            company: formdata.company,
            address_1: formdata.address_1,
            address_2: formdata.address_2,
            city: formdata.city,
            state: formdata.state,
            postcode: formdata.postcode,
            country: "CH"
        };

        var shipping2 = {
            first_name: formdata.b2_first_name,
            last_name: formdata.b2_last_name,
            company: formdata.b2_company,
            address_1: formdata.b2_address_1,
            address_2: formdata.b2_address_2,
            city: formdata.b2_city,
            state: formdata.b2_state,
            postcode: formdata.b2_postcode,
            country: "CH"
        };

        const data = {
            //customer_id: userdata.id,
            payment_method: formdata.payment_method,
            payment_method_title: formdata.pay_method_title,
            set_paid: true,
            billing: billing,
            shipping: (isUserAvail > 0) ? shipping2 : (formdata.hasOwnProperty('diffshipping')) ? shipping2 : shipping1,
            line_items: shopitems,
            shipping_lines: [
                {
                    method_id: formdata.ship_method,
                    method_title: formdata.ship_method_title
                }
            ]
        };

        if (isUserAvail > 0) {
            data.customer_id = userdata.id;
        }

        /*console.log("++++++++++++Start console++++++++++++++++");
        console.log(isUserAvail);
        console.log(data);
        return false;
        console.log("++++++++++++End Console++++++++++++++++");*/
        //return {"status": true};

        var order = await postWCApiAsync("orders", data);

        console.log(order);

        if (order.hasOwnProperty('message')) {
            return { "status": false, "code": order.data.status, "error": order.message };
        }

        return { "status": true, "code": 200, 'order_id': order.id, "order_key": order.order_key, "message": "Order Successfully Received..." };
    }
}

module.exports = Checkout;