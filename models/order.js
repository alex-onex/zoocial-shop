
Order = {

    async lists(req) {

        let customer = req.session.auth;
        inputdata = {
            customer: customer.id
        }
        let categories = await getWCApiAsync("products/categories");
        var orders = await getWCApiAsync("orders", inputdata);

         var data = {
             status: true,
             categories: utls.getNestedChildren(categories, 0),
             orders: orders
         }
         
        return data;
    },
    async orderSuccess(req) {
        var orderid = parseInt(req.params.orderid);
        var data = {};
        let categories = await getWCApiAsync("products/categories");
        var orderdetails = await getWCApiAsync("orders/" + orderid);
        console.log(orderdetails);
        if(orderdetails.code === 'woocommerce_rest_shop_order_invalid_id') {
            data = {
                status:false,
                orderdetails: orderdetails,
                categories: utls.getNestedChildren(categories, 0)
            }
        } else {
            data = {
                status:true,
                orderdetails: orderdetails,
                categories: utls.getNestedChildren(categories, 0)
            }
        }

        return data;
    }
}

module.exports = Order;