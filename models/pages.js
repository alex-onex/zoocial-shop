
Pages = {

    async faq(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/faq", data);
    },
    async about(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/about_us", data);
    },
    async zahlungsarten(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/zahlungsarten", data);
    },
    async versandkosten(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/versandkosten", data);
    },
    async ruckgabe_artikeln(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/ruckgabe_artikeln", data);
    },
    async agb(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/agb", data);
    },
    async impressum(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/impressum", data);
    },
    async datenschutz(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/datenschutz", data);
    },
    async register(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/register", data);
    },
    async kontakt(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/kontakt", data);
    },
    async medien(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/medien", data);
    },
    async sponsoring(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/sponsoring", data);
    },
    async meine_bestellungen(req, res) {
        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }
        res.render("shop/pages/meine_bestellungen", data);
    },
    async search(req, res) {

        let search = req.query.search;

        let products = await getWCApiAsync("products?search=" + search);
        //let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }

        res.render("shop/pages/search", data);

    },
    async checkout(req, res) {

        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }

        res.render("shop/pages/checkout", data);

    },
}

module.exports = Pages;