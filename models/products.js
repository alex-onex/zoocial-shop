

Products = {
    create(req) {
        var data = JSON.stringify({ "status": true, "method": 'create' });
        return data;
    },
    update(req) {
        var data = JSON.stringify({ "status": true, "method": 'update' });
        return data;
    },
    delete(req) {
        var data = JSON.stringify({ "status": true, "method": 'delete' });
        return data;
    },
    async lists(req) {

        let products = await getWCApiAsync("products");
        let categories = await getWCApiAsync("products/categories");
        let tags = await getWCApiAsync("products/tags");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0),
            tags: tags
        }

        return data;
    },
    async list(req) {

        //console.log(req.params.id);
        let product = await getWCApiAsync("products/" + req.params.id);
        let categories = await getWCApiAsync("products/categories");
        //let reviews = await getWCApiAsync("products/reviews/"+req.params.id);
        let reviews = await getWCApiAsync("products/reviews/" + parseInt(req.params.id));
        let variations = (product.type == 'variable') ? await getWCApiAsync("products/" + req.params.id + "/variations") : null;

        product.meta_data.forEach((metainfo, el)=> {
            if(metainfo.hasOwnProperty('yikes_woo_products_tabs')) {
                var tabs = metainfo;
            }
            if(metainfo.hasOwnProperty('minquantity')) {
                var mqty = metainfo;
            }
            if(metainfo.hasOwnProperty('discount')) {
                var discount = metainfo;
            }
        });

        //console.log(tabs);
        //console.log(mqty);
        //console.log(discount);

        data = {
            product: product,
            categories: utls.getNestedChildren(categories, 0),
            reviews: (reviews.data.status == 404) ? null : reviews,
            variations: variations
        }

        return data;
    },
    async singleProduct(req) {

        var tabs = {},
            minqty={}, 
            discount= {},
            variations ={},
            discountAvail = {},
            inhaltstsstoffe = {},
            zusammensetzung = {},
            futter_content = {},
            futter_image_url = {},
            product_rating = {},
            product_rating_count = {};    

        //console.log(req.params.slug);

        //  "/(\d+)[^-]*$/g"  //identify final hyphen

        reqdata = {
            slug: req.params.slug
        }

        //console.log(req.params.id);
        let product = await getWCApiAsync("products/" + parseInt(req.params.id));
        let categories = await getWCApiAsync("products/categories");
        let reviews = await getWCApiAsync("products/reviews/" + parseInt(req.params.id));
        variations = (product.type == 'variable') ? await getWCApiAsync("products/" + req.params.id + "/variations") : null;

        //console.log(product.meta_data); 

        product.meta_data.forEach((metainfo, el)=> {
            if(metainfo.key == "yikes_woo_products_tabs") {
                tabs = metainfo.value;
            }
            if(metainfo.key == "minquantity") {
                minqty = metainfo;
            }
            if(metainfo.key == "discount") {
                discount = metainfo;
            }
            if(metainfo.key == "discountAvail") {
                discountAvail = metainfo.value;
            }
            if(metainfo.key == "inhaltstsstoffe") {
                inhaltstsstoffe = metainfo;
            }
            if(metainfo.key == "zusammensetzung") {
                zusammensetzung = metainfo;
            }
            if(metainfo.key == "futter_content") {
                futter_content = metainfo;
            }
            if(metainfo.key == "futter_image_url") {
                futter_image_url = metainfo;
            }
            if(metainfo.key == "product_rating") {
                product_rating = metainfo;
            }
            if(metainfo.key == "product_rating_count") {
                product_rating_count = metainfo;
            }
        });

        data = {
            product: product,
            categories: utls.getNestedChildren(categories, 0),
            reviews: (reviews.data.status == 404) ? null : reviews,
            variations: (product.type == 'variable') ? variations.reverse() : {},
            tabs: tabs,
            minqty: minqty,
            discount: discount,
            discountAvail: discountAvail,
            inhaltstsstoffe: inhaltstsstoffe,
            zusammensetzung: zusammensetzung,
            futter_content: futter_content,
            futter_image_url: futter_image_url,
            product_rating: product_rating,
            product_rating_count: product_rating_count,
        }

        return data;
    },
    async tags(req) {

        console.log(req.params);

        var slug = req.params.slug;
        var id = req.params.id;

        let rqdata = {
            status: 'publish',
            per_page: 9,
            tag: id,
            page: 1,
            orderby: "date",
            order: "desc"
        }

        let products = await getWCApiAsync("products", rqdata);
        let categories = await getWCApiAsync("products/categories");
        let tags = await getWCApiAsync("products/tags");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0),
            tags: tags,
            tag_id: id,
            customeScript: {enable: true}
        }

        //res.render("shop/pages/brand", data);
        return data;

    },
    async createReview(req) {

        let formdata = utls.parseQuery(req.body.formdata);

        const postData = {
            product_id: formdata.product_id,
            review: formdata.review,
            reviewer: formdata.reviewer,
            reviewer_email: formdata.reviewer_email,
            rating: formdata.rating
        };

        let reviews = await postWCApiAsync("products/reviews", postData);

        data = {
            status: true,
            reviews: reviews,
            message: "Thank You For your Review..."
        }

        return data;

    },
    async filter(req) {

        //let formdata = utls.parseQuery(req.body.formdata);
        console.log(req.query);
        console.log(req.params);
        var azflt = req.query.s;
        var noflt = req.query.p
        var page = req.query.page
        var price = req.query.price
        var category_id = req.query.cid
        var tag_id = req.query.tag_id

        reqdata = {
            status: "publish",
            //per_page: (parseInt(noflt) > 0) ? noflt : 9,
            per_page: 9,
            page: (page) ? parseInt(page) : 1,
        };

        if(parseInt(category_id) > 0) {
            reqdata.category = category_id;
        }

        if(parseInt(tag_id) > 0) {
            reqdata.tag = tag_id;
        }

        switch (azflt) {
            case "date-asc":
                reqdata.orderby = 'date';
                reqdata.order = "asc";
                break;
            case "price-desc":
                reqdata.orderby = 'price';
                reqdata.order = "desc";
                break;
            case "price-asc":
                reqdata.orderby = 'price';
                reqdata.order = "asc";
                break;
            case "popularity":
                reqdata.orderby = 'popularity';
                break;
            default:
                reqdata.orderby = 'date';
                reqdata.order = "desc";
        }

        if (price !== "") {
            console.log('price allowed:');
            if (utls.isNotEmpty(price) && price == '-20') {
                reqdata.min_price = "";
                reqdata.max_price = "20";
                console.log('price:' + price);
            }
            if (utls.isNotEmpty(price) && price == '20-100') {
                reqdata.min_price = '20';
                reqdata.max_price = '100';
                console.log('price:' + price);
            }
            if (utls.isNotEmpty(price) && price == "100+") {
                reqdata.min_price = '100';
                reqdata.max_price = "";
                console.log('price:' + price);
            }
        }

        console.log(reqdata);

        /*reqdata.orderby = 'price';
        reqdata.order = "desc";
        //reqdata.meta_key = "price";*/

        var products = await getWCApiAsync("products", reqdata);

        if (products.length > 0) {
            data = {
                status: true,
                products: products,
                page: reqdata.page
            }
        } else {
            data = {
                status: false,
                products: null,
                page: null
            }
        }

        return data;
    },
    async homePageNew(req) {
        let categories = await getWCApiAsync("products/categories");
        data = {
            categories: utls.getNestedChildren(categories, 0)
        }
        return data;                        
    }
}

module.exports = Products;