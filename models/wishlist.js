
Wishlist = {
    async create(req) {

        //res.setHeader('Content-Type', 'application/json');
        console.log(req.body);
        let favid = req.body.id;
        let favitems = req.session.favitems;

        let ckval = _.find(favitems, function (f) {
            return (f == favid);
        });

        if (!ckval || typeof ckval == 'undefined') {
            req.session.favitems.push(favid);
        }

        var data = JSON.stringify({ "status": true, "method": 'create', 'items': req.session.favitems });

        return data;

        //res.send(JSON.stringify({ "status": true, "method": 'create', 'items': req.session.favitems }));

    },
    delete(req) {

        //res.setHeader('Content-Type', 'application/json');

        let favid = req.body.id;
        var favitems = req.session.favitems;
        req.session.favitems = _.without(favitems, favid);

        for (var i = 0; i < favitems.length; i++) {
            if (favitems[i] == favid) {
                req.session.favitems.splice(favid, 1);
            }
        }

        console.log(req.session.favitems);

        var data = JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.favitems });
        return data;

        //res.send(JSON.stringify({ "status": 200, "method": 'delete', 'items': req.session.favitems }));
    },
    async lists(req) {

        inputdata = {
            include: req.session.favitems
        }

        let products = await getWCApiAsync("products", inputdata);
        let categories = await getWCApiAsync("products/categories");

        data = {
            products: products,
            categories: utls.getNestedChildren(categories, 0)
        }

        return data;
        //res.render("shop/pages/wishlist", data);
    },
    async list(req) {
        //res.setHeader('Content-Type', 'application/json');
        var data = JSON.stringify({ "status": 200, "method": 'list' });
        return data;
        //res.send(JSON.stringify({ "status": 200, "method": 'list' }));
    },
    async module(req) {
        //res.setHeader('Content-Type', 'application/json');

        let favitems = req.session.favitems;

        if (favitems.length > 0 && typeof favitems != 'undefined') {

            inputdata = {
                include: req.session.favitems
            }

            let favproducts = await getWCApiAsync("products", inputdata);

            var data = {
                favproducts: favproducts,
                favcount: req.session.favitems.length
            }

            //return data;
            //res.render("shop/modules/wishlist", data);

        } else {
            var data = `<div class="form-header">
                       <h4>Lieblingsartikel</h4>
                       <div id="favClose" class="slideFormClose">×</div>
                        </div>
                        <h3>Keine Favoriten hinzugefügt ...</h3>
                        `;
            //res.send(str);
        }
        return data;
    }
}

module.exports = Wishlist;