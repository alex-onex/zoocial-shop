var express = require('express');
var router = express.Router();

var Products = require('../controllers/products');
var Categories = require('../controllers/categories');
var Cart = require('../controllers/cart');
var Wishlist = require('../controllers/wishlist');
var Authorization = require('../controllers/authorization');
var Checkout = require('../controllers/checkout');
var Order = require('../controllers/order');
var Pages = require('../models/pages');

//controllers
var Testprod = require('../controllers/testprod');

//Products
router.post('/products', Products.create);
router.get('/productupdate', Products.filter);
router.get('/products', Products.lists);
router.get('/products/:id', Products.list);
router.get('/product/:slug/:id', Products.singleProduct);
router.put('/products/:id', Products.update);
router.delete('/products/:id', Products.delete);
router.get('/home', Products.homePageNew);

//Reviews
router.post('/review', Products.createReview);

//brands
router.get("/brand/:slug/:id", Products.tags);

//Categories
router.post('/categories', Categories.create);
router.get('/categories', Categories.lists);
router.get('/categories/:id', Categories.list);
router.get('/category/:slug/:id', Categories.singleCategory);
router.put('/categories/:id', Categories.update);
router.delete('/categories/:id', Categories.delete);

//Wishlist
router.post('/wishlist', Wishlist.create);
router.get('/wishlist', Wishlist.lists);
router.delete('/wishlist', Wishlist.delete);
router.get('/wishmodule', Wishlist.module);

//Cart
router.post('/cart', Cart.create);
router.get('/cart', Cart.lists);
router.delete('/cart', Cart.delete);
router.get('/cartmodule', Cart.module);
router.post('/cartupdate', Cart.update);
router.get('/cartupdate', Cart.update);
//router.get('/dmodule', Cart.module);

//Pages
router.get('/faq', Pages.faq);
router.get('/about_us', Pages.about);
router.get('/zahlungsarten', Pages.zahlungsarten);
router.get('/ruckgabe_artikeln', Pages.ruckgabe_artikeln);
router.get('/agb', Pages.agb);
router.get('/impressum', Pages.impressum);
router.get('/datenschutz', Pages.datenschutz);
router.get('/kontakt', Pages.kontakt);
router.get('/medien', Pages.medien);
router.get('/sponsoring', Pages.sponsoring);
router.get('/meine_bestellungen', Pages.meine_bestellungen);
router.get('/search', Pages.search);

//Authorization
router.get('/register', Authorization.authChecker, Authorization.register);
router.post('/register', Authorization.register);
router.get('/login', Authorization.authChecker, Authorization.login);
router.post('/login', Authorization.login);
router.get('/logout', Authorization.logout);
router.get('/myaccount', Authorization.myAccount);
router.post('/updateaccount', Authorization.updateAccount);
router.get('/usermenus', Authorization.userMenus);

//Checkout
router.get('/checkout', Checkout.checkout);
router.post('/order', Checkout.order);

//Orders
router.get('/order-received/:orderid', Order.orderSuccess);
router.get('/view-order/:orderid', Authorization.isAuthorize, Order.viewOrder);
router.get('/orders', Authorization.isAuthorize, Order.lists);

router.get('/testlists', Testprod.lists);

module.exports = router;