var crypto = require('crypto');

utls = {
    isEmpty: function (val) {
        if (val != '' && val != 0 && typeof (val) != 'undefined' && typeof (val) != 'null' && val != null) {
            return true;
        } else {
            return false;
        }
    },
    isNotEmpty: function (val) {
        if (val != '' && val !== 0 && typeof (val) != 'undefined' && typeof (val) != 'null' && val != null) {
            return true;
        } else {
            return false;
        }
    },
    isNotdefined: function (val) {
        if (val == '' || typeof (val) == 'undefined' || typeof (val) == 'null' && val || null) {
            return false;
        } else {
            return true;
        }
    },
    checkEmpty: function (val) {
        if (val == '' || val == 0 || typeof (val) == 'undefined' || typeof (val) == 'null' || val == null) {
            return true;
        } else {
            return false;
        }
    },
    saltHashPassword: function (password, pslt = '', slt = true) {
        var pswd = '';
        var slth = [];
        let Salt = crypto.randomBytes(16).toString('base64');
        //let Hash = crypto.createHmac('sha512', Salt).update(password).digest("base64");
        let Hash = crypto.createHmac('sha512', Salt).update(password).digest("base64");

        if (slt) {
            slth.push(Salt);
            slth.push(Salt + "$" + Hash);
        } else {
            if (pslt == '' || pslt == null || typeof (pslt) == 'undefined') {
                var splt = password.split('$');
                slth.push(splt[0]);
                slth.push(splt[1]);
            } else {
                Hash = crypto.createHmac('sha512', pslt).update(password).digest("base64");
                slth.push(pslt);
                slth.push(pslt + "$" + Hash);
            }
        }
        return slth;
    },
    getToken: function () {
        return crypto.randomBytes(16).toString('base64');
    },
    getNestedChildren: function (arr, parent) {
        var children = [];
        for (var i = 0; i < arr.length; ++i) {
            if (arr[i].parent == parent) {
                var grandChildren = this.getNestedChildren(arr, arr[i].id)

                if (grandChildren.length) {
                    arr[i].children = grandChildren;
                }
                children.push(arr[i]);
            }
        }
        return children;
    },
    getCategories: function (callback) {
        // List products
        wcapi.get("products/categories").then((response) => {
            callback(true, this.getNestedChildren(response.data, 0));
        }).catch((error) => {
            callback(false, error);
        });

    },
    joinObjects: function () {
        var idMap = {};
        for (let i = 0; i < arguments.length; i++) {
            for (let j = 0; j < arguments[i].length; j++) {
                let currentID = arguments[i][j]['id'];
                if (!idMap[currentID]) {
                    idMap[currentID] = {};
                }
                // Iterate over properties of objects in arrays
                for (key in arguments[i][j]) {
                    idMap[currentID][key] = arguments[i][j][key];
                }
            }
        }

        // push properties of idMap into an array
        let newArray = [];
        for (property in idMap) {
            newArray.push(idMap[property]);
        }
        return newArray;
    },
    parseQuery: function (queryString) {
        var query = {};
        //var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
        var pairs = queryString.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            var pname = decodeURIComponent(pair[0]);
            var pval = pair[1].replace(/\+/g, '%20');
            query[pname] = decodeURIComponent(pval || '');
        }
        return query;
    },
    strToLowerCase: function (str) {
        return str.toLowerCase();
    },
    strToUpperCase: function (str) {
        return str.toUpperCase();
    },
    async postData(url, inputData = {}) {

        var method = inputData.method;
        try {
            const response = await fetch(url, {
                method: method, // or 'PUT'
                body: JSON.stringify(inputData.data), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            const jsondata = await response.json();
            return jsondata;

        } catch (error) {
            console.error('Error:', error);
        }
    },
    swissStates() {
        return {
            "AG": "Aargau",
            "AR": "Appenzell Ausserrhoden",
            "AI": "Appenzell Innerrhoden",
            "BL": "Basel-Landschaft",
            "BS": "Basel-Stadt",
            "BE": "Bern",
            "FR": "Fribourg",
            "GE": "Geneva",
            "GL": "Glarus",
            "GR": "Graubünden",
            "JU": "Jura",
            "LU": "Luzern",
            "NE": "Neuchâtel",
            "NW": "Nidwalden",
            "OW": "Obwalden",
            "SH": "Schaffhausen",
            "SZ": "Schwyz",
            "SO": "Solothurn",
            "SG": "St. Gallen",
            "TG": "Thurgau",
            "TI": "Ticino",
            "UR": "Uri",
            "VS": "Valais",
            "VD": "Vaud",
            "ZG": "Zug",
            "ZH": "Zürich",
        };
    },
    priceFormat(price) {
        price = parseFloat(price);
        return price.toFixed(2);
    },
    discountPrice(price, discount) {
        price = parseFloat(price - ((price / 100) * discount));
        return price.toFixed(2);
    },
    discountPercentageValue(price, discount) {
        price = parseFloat((price / 100) * discount);
        return price.toFixed(2);
    },
    imageOptimize(name, size = "m") {
        var filename = path.parse(name);
        var mg = null;
        switch (size) {
            case "t":
                mg = filename.dir + "/" + filename.name + "-100x100" + filename.ext;
                break;
            case "s":
                mg = filename.dir + "/" + filename.name + "-150x150" + filename.ext;
                break;
            case "m":
                mg = filename.dir + "/" + filename.name + "-200x300" + filename.ext;
                break;
            case "l":
                mg = filename.dir + "/" + filename.name + "-324x324" + filename.ext;
                break;
            case "xl":
                mg = filename.dir + "/" + filename.name + "-416x624" + filename.ext;
                break;
            case "o":
                mg = name;
                break;
            default:
                mg = name;
        }

        return mg;
    },
}

module.exports = utls;